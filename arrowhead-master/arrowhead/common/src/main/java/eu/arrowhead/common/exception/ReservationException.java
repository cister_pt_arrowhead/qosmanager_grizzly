package eu.arrowhead.common.exception;

public class ReservationException extends Exception {

	public ReservationException() {
		super();
	}

	public ReservationException(String message) {
		super(message);
	}

}
