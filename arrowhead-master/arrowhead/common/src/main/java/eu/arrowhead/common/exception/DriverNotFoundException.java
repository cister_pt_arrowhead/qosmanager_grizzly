package eu.arrowhead.common.exception;

/**
 * Related to the drivers of each communication protocol. Whenever the
 * DriverFactory doesn't recognize the type, this exception will be launched.
 *
 * @author Paulo
 *
 */
public class DriverNotFoundException extends Exception {

    public DriverNotFoundException(String message) {
        super(message);
    }

}
