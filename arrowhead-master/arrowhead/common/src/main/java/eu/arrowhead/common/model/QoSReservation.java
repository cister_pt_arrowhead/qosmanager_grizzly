package eu.arrowhead.common.model;

import java.util.Map;

public class QoSReservation {

	private Map<String, String> requestedQoS;
	private Map<String, String> parameters;

	public QoSReservation(Map<String, String> requestedQoS, Map<String, String> parameters) {
		super();
		this.requestedQoS = requestedQoS;
		this.parameters = parameters;
	}

	public Map<String, String> getRequestedQoS() {
		return requestedQoS;
	}

	public void setRequestedQoS(Map<String, String> requestedQoS) {
		this.requestedQoS = requestedQoS;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

}
