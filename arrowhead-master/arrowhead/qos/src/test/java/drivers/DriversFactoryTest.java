/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drivers;

import java.lang.reflect.Method;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import qosmanager.drivers.FTTSE;

/**
 *
 * @author Paulo
 */
public class DriversFactoryTest {

	public DriversFactoryTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getInstance method, of class DriversFactory.
	 */
	@Test
	public void testGetInstance() {
		System.out.println("getInstance");
		DriversFactory result = DriversFactory.getInstance();
		if (result != null) {
			assertEquals(1, 1);
		} else {
			assertEquals(1, 0);
		}
	}

	/**
	 * Test of findClass method, of class DriversFactory.
	 */
	@Test
	public void testFindClass() throws Exception {
		System.out.println("findClass");
		String communicationProtocol = "FTTSE";
		DriversFactory instance = new DriversFactory();
		FTTSE expResult = new FTTSE();
		Class result = instance.findClass(communicationProtocol);
		assertEquals(expResult.getClass(), result);

	}

	/**
	 * Test of findClass method, of class DriversFactory.
	 */
	@Test
	public void testFindClass1() throws Exception {
		System.out.println("findClass");
		String communicationProtocol = "FTT-SE";
		DriversFactory instance = new DriversFactory();
		FTTSE expResult = new FTTSE();
		try {
			Class result = instance.findClass(communicationProtocol);
		} catch (ClassNotFoundException e) {
			assertEquals(1, 1);
			return;
		}
		fail("Class Not Found Error !");
	}

	/**
	 * Test of findMethod method, of class DriversFactory.
	 */
	@Test
	public void testFindMethod() throws Exception {
		System.out.println("findMethod");
		String communicationProtocol = "FTTSE";

		DriversFactory instance = new DriversFactory();
		Method expResult = FTTSE.class.
			getDeclaredMethod("reserveQoS", ReservationInfo.class);
		Class cls = instance.findClass(communicationProtocol);
		Method result = instance.findMethod(cls);
		assertEquals(expResult, result);

	}

}
