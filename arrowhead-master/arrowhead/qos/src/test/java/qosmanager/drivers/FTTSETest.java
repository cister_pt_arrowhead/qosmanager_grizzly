/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qosmanager.drivers;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo
 */
public class FTTSETest {

	public FTTSETest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of testCalculateSize method, of class FTTSE.
	 */
	@Test
	public void testCalculateSize() {
		System.out.println("calculateSize");
		Integer mtu = 1500;
		FTTSE instance = new FTTSE();
		int expResult = 7500;
		int result = instance.calculateSize(mtu);
		assertEquals(expResult, result);

	}

	/**
	 * Test of generateCommands method, of class FTTSE. With QoS, bandwitdh
	 * 1500B/s, delay 20 ms.
	 */
	@Test
	public void testGenerateCommands1() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Integer mtu = 1500;
		Map<String, String> requestedQoS = new HashMap<>();
		requestedQoS.put("bandwidht", "1500");
		requestedQoS.put("delay", "20");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("PERIOD", "1");
		expResult.put("SIZE", "7500");
		expResult.put("ID", "1");
		expResult.put("SYNCHRONOUS", "0");

		Map<String, String> result = instance.
			generateCommands(streamID, elementaryCycle, mtu, requestedQoS);
		assertEquals(expResult, result);
	}

	/**
	 * Test of generateCommands method, of class FTTSE. No QoS.
	 */
	@Test
	public void testGenerateCommands2() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Integer mtu = 1500;
		Map<String, String> requestedQoS = new HashMap<>();

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("PERIOD", "5");
		expResult.put("SIZE", "7500");
		expResult.put("ID", "1");
		expResult.put("SYNCHRONOUS", "3");

		Map<String, String> result = instance.
			generateCommands(streamID, elementaryCycle, mtu, requestedQoS);
		assertEquals(expResult, result);
	}

	/**
	 * Test of generateCommands method, of class FTTSE. With invalid QoS.
	 */
	@Test
	public void testGenerateCommands3() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Integer mtu = 1500;
		Map<String, String> requestedQoS = new HashMap<>();
		requestedQoS.put("ERROR", "1500");
		requestedQoS.put("NULL", "20");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("PERIOD", "5");
		expResult.put("SIZE", "7500");
		expResult.put("ID", "1");
		expResult.put("SYNCHRONOUS", "0");

		Map<String, String> result = instance.
			generateCommands(streamID, elementaryCycle, mtu, requestedQoS);
		assertEquals(expResult, result);
	}

	/**
	 * Test of generateCommands method, of class FTTSE. With QoS, bandwitdh
	 * 1500B/s, delay 20 ms.
	 */
	@Test
	public void testGenerateCommands4() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Integer mtu = 1500;
		Map<String, String> requestedQoS = new HashMap<>();
		requestedQoS.put("bandwidht", "");
		requestedQoS.put("delay", "");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("PERIOD", "1");
		expResult.put("SIZE", "7500");
		expResult.put("ID", "1");
		expResult.put("SYNCHRONOUS", "0");

		try {
			Map<String, String> result = instance.
				generateCommands(streamID, elementaryCycle, mtu, requestedQoS);
			assertEquals(expResult, result);
		} catch (NumberFormatException e) {
			assertEquals(1, 1);
		}
	}

	/**
	 * Test of generateCommands method, of class FTTSE. With QoS, invalid
	 * bandwitdh , delay 20 ms.
	 */
	@Test
	public void testGenerateCommands5() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Integer mtu = 1500;
		Map<String, String> requestedQoS = new HashMap<>();
		requestedQoS.put("bandwidht", "X");
		requestedQoS.put("delay", "10");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("PERIOD", "1");
		expResult.put("SIZE", "7500");
		expResult.put("ID", "1");
		expResult.put("SYNCHRONOUS", "0");

		try {
			Map<String, String> result = instance.
				generateCommands(streamID, elementaryCycle, mtu, requestedQoS);
			assertEquals(expResult, result);
		} catch (IllegalArgumentException e) {
			assertEquals(1, 1);
		}

	}

	/**
	 * Test of generateCommands method, of class FTTSE. With QoS, invalid
	 * bandwitdh , delay 20 ms.
	 */
	@Test
	public void testGenerateCommands6() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Integer mtu = 1500;
		Map<String, String> requestedQoS = new HashMap<>();
		requestedQoS.put("bandwidht", "1500");
		requestedQoS.put("delay", "X");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("PERIOD", "1");
		expResult.put("SIZE", "7500");
		expResult.put("ID", "1");
		expResult.put("SYNCHRONOUS", "0");

		try {
			Map<String, String> result = instance.
				generateCommands(streamID, elementaryCycle, mtu, requestedQoS);
			assertEquals(expResult, result);
		} catch (NumberFormatException e) {
			assertEquals(1, 1);
		}
	}
}
