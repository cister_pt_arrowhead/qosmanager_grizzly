package database.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entity class for storing Arrowhead Services in the database. The
 * "service_group" and service_definition" columns must be unique together.
 */
@Entity
@Table(name = "arrowhead_service", uniqueConstraints = {
	@UniqueConstraint(columnNames = {"service_group", "service_definition"})})
@XmlRootElement
public class ArrowheadService {

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "service_group")
	private String serviceGroup;

	@Column(name = "service_definition")
	private String serviceDefinition;

	@ElementCollection(fetch = FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<String> interfaces = new ArrayList<String>();

	public ArrowheadService() {
	}

	public ArrowheadService(String serviceGroup, String serviceDefinition,
							List<String> interfaces) {
		this.serviceGroup = serviceGroup;
		this.serviceDefinition = serviceDefinition;
		this.interfaces = interfaces;
	}

	@XmlTransient
	public int getId() {
		return id;
	}

	/**
	 * Set ID.
	 *
	 * @param id Indentifiers of the object.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Get Service Group.
	 *
	 * @return Returns String.
	 */
	public String getServiceGroup() {
		return serviceGroup;
	}

	/**
	 * Set Service Group.
	 *
	 * @param serviceGroup String serviceGroup.
	 */
	public void setServiceGroup(String serviceGroup) {
		this.serviceGroup = serviceGroup;
	}

	/**
	 * Get Service Definition.
	 *
	 * @return Returns String.
	 */
	public String getServiceDefinition() {
		return serviceDefinition;
	}

	/**
	 * Set Service Definition.
	 *
	 * @param serviceDefinition ServiceDefinition.
	 */
	public void setServiceDefinition(String serviceDefinition) {
		this.serviceDefinition = serviceDefinition;
	}

	/**
	 * Get service definition.
	 *
	 * @return Returns list of string.
	 */
	public List<String> getInterfaces() {
		return interfaces;
	}

	/**
	 * Set Interfaces.
	 *
	 * @param interfaces Set list of interfaces.
	 */
	public void setInterfaces(List<String> interfaces) {
		this.interfaces = interfaces;
	}

	/**
	 * Add one interface.
	 *
	 * @param oneInterface
	 */
	public void setInterfaces(String oneInterface) {
		List<String> interfaces = new ArrayList<String>();
		interfaces.add(oneInterface);
		this.interfaces = interfaces;
	}

	/**
	 * Determines if object is valid.
	 *
	 * @return Returns true if is valid.
	 */
	public boolean isValid() {
		if (serviceGroup == null || serviceDefinition == null) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "(" + serviceGroup + ":" + serviceDefinition + ")";
	}

}
