package drivers;

public interface IQoSDriver {

	/**
	 * Configures a stream between a provider and a consumer.
	 *
	 * @param info Necessary information to the driver.
	 * @return Returns the stream configuration parameters.
	 */
	public ReservationResponse reserveQoS(ReservationInfo info);

}
