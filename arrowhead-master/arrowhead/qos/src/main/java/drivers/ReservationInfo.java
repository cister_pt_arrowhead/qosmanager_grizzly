/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drivers;

import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;
import java.util.Map;

/**
 *
 * @author Paulo
 */
public class ReservationInfo {

	Map<String, String> networkConfiguration;
	ArrowheadSystem provider;
	ArrowheadSystem consumer;
	ArrowheadService service;
	Map<String, String> commands;
	Map<String, String> requestedQoS;

	public ReservationInfo() {
	}

	public ReservationInfo(Map<String, String> networkConfiguration,
						   ArrowheadSystem provider, ArrowheadSystem consumer,
						   ArrowheadService service,
						   Map<String, String> commands,
						   Map<String, String> requestedQoS) {
		this.networkConfiguration = networkConfiguration;
		this.provider = provider;
		this.consumer = consumer;
		this.service = service;
		this.commands = commands;
		this.requestedQoS = requestedQoS;
	}

	public Map<String, String> getNetworkConfiguration() {
		return networkConfiguration;
	}

	public void setNetworkConfiguration(
		Map<String, String> networkConfiguration) {
		this.networkConfiguration = networkConfiguration;
	}

	public ArrowheadSystem getProvider() {
		return provider;
	}

	public void setProvider(ArrowheadSystem provider) {
		this.provider = provider;
	}

	public ArrowheadSystem getConsumer() {
		return consumer;
	}

	public void setConsumer(ArrowheadSystem consumer) {
		this.consumer = consumer;
	}

	public ArrowheadService getService() {
		return service;
	}

	public void setService(ArrowheadService service) {
		this.service = service;
	}

	public Map<String, String> getCommands() {
		return commands;
	}

	public void setCommands(Map<String, String> commands) {
		this.commands = commands;
	}

	public Map<String, String> getRequestedQoS() {
		return requestedQoS;
	}

	public void setRequestedQoS(Map<String, String> requestedQoS) {
		this.requestedQoS = requestedQoS;
	}

}
