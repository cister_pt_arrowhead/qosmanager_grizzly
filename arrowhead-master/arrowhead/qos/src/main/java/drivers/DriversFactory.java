package drivers;

import eu.arrowhead.common.exception.DriverNotFoundException;
import eu.arrowhead.common.exception.ReservationException;
import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class DriversFactory {

	private static DriversFactory instance;
	Class[] paramVerificationInfo = new Class[1];

	protected DriversFactory() {
		//
		super();
		//VerificationInfo parameter
		paramVerificationInfo[0] = ReservationInfo.class;
	}

	/**
	 * Returns a instance from this singleton class.
	 *
	 * @return
	 */
	public static DriversFactory getInstance() {
		if (instance == null) {
			instance = new DriversFactory();
		}
		return instance;
	}

	/**
	 *
	 * @param networkType Network type (ex. fttse)
	 * @param networkConfiguration Network configuration parameters on a map.
	 * @param provider ArrowheadSystem.
	 * @param consumer ArrowheadSystem.
	 * @param service ArrowheadService.
	 * @param commands Map of the selected commands from the user.
	 * @param requestedQoS Map of the desired requestedQoS.
	 * @return Returns the generatedCommands from the QoSDriver.
	 * @throws ReservationException The StreamConfiguration found an error.
	 * @throws DriverNotFoundException The selected type doesnt have an assigned
	 * driver.
	 */
	public Map<String, String> generateCommands(String communicationProtocol,
												Map<String, String> networkConfiguration,
												ArrowheadSystem provider,
												ArrowheadSystem consumer,
												ArrowheadService service,
												Map<String, String> commands,
												Map<String, String> requestedQoS) throws ReservationException, DriverNotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {

		// Class Invoking
		Class cls = findClass(communicationProtocol);
		Object obj = cls.newInstance();
		// Method Invoking
		Method method = findMethod(cls);

		Map<String, String> streamConfiguration = (Map<String, String>) method.
			invoke(obj, new ReservationInfo(networkConfiguration,
											provider, consumer, service, commands, requestedQoS));

		if (streamConfiguration == null) {
			throw new ReservationException();
		}

		return streamConfiguration;

	}

	protected Class findClass(String communicationProtocol) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		// Class Invoking
		Class cls;
		cls = Class.forName("qosmanager.drivers." + communicationProtocol.
			toUpperCase());
		return cls;
	}

	protected Method findMethod(Class cls) throws NoSuchMethodException {
		Method method = cls.
			getDeclaredMethod("reserveQoS", paramVerificationInfo);
		return method;
	}

}
