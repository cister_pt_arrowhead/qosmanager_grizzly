package qosmanager.algorithms;

import algorithms.IVerifierAlgorithm;
import algorithms.VerificationInfo;
import algorithms.VerificationResponse;
import eu.arrowhead.common.model.messages.QoSVerifierResponse;

public class FTTSE implements IVerifierAlgorithm {

	private final String BANDWIDTH = "bandwidth";
	private final String DELAY = "delay";

	public FTTSE() {
		super();
	}

	@Override
	public VerificationResponse verifyQoS(VerificationInfo info) {
		QoSVerifierResponse response = new QoSVerifierResponse();
		response.setResponse(true);

		return new VerificationResponse(true, null);
	}

}
