package factories;

import database.IQoSRepository;
import database.QoSRepositoryImpl;
import database.model.Message_Stream;
import database.model.QoS_Resource_Reservation;
import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.messages.QoSReservationForm;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QoSFactory {

	private static QoSFactory instance;
	private IQoSRepository repo;

	protected QoSFactory() {
		super();
		repo = new QoSRepositoryImpl();
	}

	/**
	 * Returns a instance from this singleton class.
	 *
	 * @return
	 */
	public static QoSFactory getInstance() {
		if (instance == null) {
			instance = new QoSFactory();
		}
		return instance;
	}

	public IQoSRepository getRepo() {
		return repo;
	}

	public void setRepo(IQoSRepository repo) {
		this.repo = repo;
	}

	/**
	 * Return all the QoS reservations of the selected system.
	 *
	 * @param provider ArrowheadSystem.
	 *
	 * @return Returns all the QoSReservations.
	 */
	public List<QoS_Resource_Reservation> getQoSReservationsFromArrowheadSystem(
		ArrowheadSystem provider) {
		return repo.
			getQoSReservationsFromArrowheadSystem(convertFromDTO(provider));
	}

	/**
	 * Saves a messagae stream.
	 *
	 * @param provider ArrowheadSystem that provides the service.
	 * @param consumer ArrowheadSystem that consumes the service.
	 * @param service ArrowheadService is the service that will be consumed and
	 * provided.
	 * @param qualityOfService Requestet QoS.
	 * @param messageConfigurationParameters Stream configuration parameters
	 * between consumer and provider.
	 * @param type Network Type.
	 * @return Return true when successful.
	 */
	public boolean saveMessageStream(ArrowheadSystem provider,
									 ArrowheadSystem consumer,
									 ArrowheadService service,
									 Map<String, String> qualityOfService,
									 Map<String, String> messageConfigurationParameters,
									 String type) {

		Message_Stream m = new Message_Stream(convertFromDTO(service), convertFromDTO(consumer),
											  convertFromDTO(provider), qualityOfService, messageConfigurationParameters, type);
		if (repo.saveMessageStream(m) == null) {
			return false;
		}
		return true;
	}

	/**
	 * Get QoSReserations from a filter that contains a qos specification.
	 *
	 * @param filter QoS specification (ex. bandwidth, delay).
	 * @return Returns QoSReservation.
	 */
	public List<QoSReservationForm> getQoSReservationFromFilter(
		Map<String, String> filter) {
		List<QoSReservationForm> output = new ArrayList<>();
		for (Message_Stream m : repo.
			getQoS_Resource_ReservationsFromFilter(filter)) {
			output.
				add(new QoSReservationForm(convertToDTO(m.getService()), convertToDTO(m.
										   getProvider()),
										   convertToDTO(m.getConsumer()), m.
										   getQualityOfService().
										   getQosParameters()));
		}
		return output;
	}

	/**
	 * Get all qos reservations.
	 *
	 * @return Returns list of qos reservations.
	 */
	public List<QoS_Resource_Reservation> getAllQoS_Resource_Reservations() {
		return repo.getAllQoS_Resource_Reservations();
	}

	/**
	 * Get all messages streams.
	 *
	 * @return Returns list of MessageStream.
	 */
	public List<Message_Stream> getAllMessage_Streams() {
		return repo.getAllMessage_Streams();
	}

	/**
	 * Get all arrowhead systems.
	 *
	 * @return Returns list of arrowhead systems.
	 */
	public List<ArrowheadSystem> getAllArrowheadSystems() {
		return convertToDTO_List(repo.getAllArrowheadSystems());
	}

	/**
	 * Get all services.
	 *
	 * @return Returns list of services.
	 */
	public List<ArrowheadService> getAllArrowheadServices() {
		return convertToDTO_ArrowheadServices(repo.getAllArrowheadServices());
	}

	/**
	 * Get MessageStream from ID.
	 *
	 * @param messageStream Message Stream to be searched on db.
	 * @return Returns the message stream found on the db.
	 */
	public Message_Stream getMessage_Stream(Message_Stream messageStream) {
		return repo.getMessage_Stream(messageStream);
	}

	/**
	 * Get arrowhead system
	 *
	 * @param system ArrowheadSystem to be searched on db.
	 * @return Returns the system found on the db.
	 */
	public ArrowheadSystem getArrowheadSystem(ArrowheadSystem system) {
		return convertToDTO(repo.getArrowheadSystem(convertFromDTO(system)));
	}

	/**
	 * Get service.
	 *
	 * @param service ArrowheadService to be searched on db.
	 * @return Returns a arrowheadService.
	 */
	public ArrowheadService getArrowheadService(ArrowheadService service) {
		return convertToDTO(repo.getArrowheadService(convertFromDTO(service)));
	}

	/**
	 * Save a stream.
	 *
	 * @param messageStream Message Stream to be saved.
	 * @return Returns the messageStream with the ID.
	 */
	public Message_Stream saveMessageStream(Message_Stream messageStream) {
		return repo.saveMessageStream(messageStream);
	}

	/**
	 * Delete a stream.
	 *
	 * @param messageStream Message stream to be deleted.
	 * @return Returns true when the stream was successfully deleted.
	 */
	public boolean deleteMessageStream(Message_Stream messageStream) {
		return repo.deleteMessageStream(messageStream);
	}

	/**
	 * Delete a system.
	 *
	 * @param system System to be deleted.
	 * @return Returns true when the system was successfully deleted.
	 */
	public boolean deleteArrowheadSystem(ArrowheadSystem system) {
		return repo.deleteArrowheadSystem(convertFromDTO(system));
	}

	/**
	 *
	 * @param service
	 * @return
	 */
	public boolean deleteArrowheadService(ArrowheadService service) {
		return repo.deleteArrowheadService(convertFromDTO(service));
	}

	protected static database.model.ArrowheadSystem convertFromDTO(
		eu.arrowhead.common.model.ArrowheadSystem in) {

		database.model.ArrowheadSystem out = new database.model.ArrowheadSystem();

		out.setAuthenticationInfo(in.getAuthenticationInfo());
		out.setAddress(in.getAddress());
		out.setPort(in.getPort());
		out.setSystemGroup(in.getSystemGroup());
		out.setSystemName(in.getSystemName());

		return out;
	}

	protected static eu.arrowhead.common.model.ArrowheadSystem convertToDTO(
		database.model.ArrowheadSystem in) {

		eu.arrowhead.common.model.ArrowheadSystem out = new eu.arrowhead.common.model.ArrowheadSystem();

		out.setAuthenticationInfo(in.getAuthenticationInfo());
		out.setAddress(in.getAddress());
		out.setPort(in.getPort());
		out.setSystemGroup(in.getSystemGroup());
		out.setSystemName(in.getSystemName());

		return out;
	}

	protected static List<ArrowheadSystem> convertToDTO_List(
		List<database.model.ArrowheadSystem> in) {
		if (in == null) {
			return null;
		}
		List<ArrowheadSystem> out = new ArrayList<>();
		for (database.model.ArrowheadSystem system : in) {
			out.add(convertToDTO(system));
		}

		return out;
	}

	protected static List<database.model.ArrowheadSystem> convertFromDTO_List(
		List<ArrowheadSystem> in) {
		if (in == null) {
			return null;
		}
		List<database.model.ArrowheadSystem> out = new ArrayList<>();
		for (ArrowheadSystem system : in) {
			out.add(convertFromDTO(system));
		}

		return out;
	}

	protected static database.model.ArrowheadService convertFromDTO(
		eu.arrowhead.common.model.ArrowheadService in) {

		database.model.ArrowheadService out = new database.model.ArrowheadService();

		out.setInterfaces(in.getInterfaces());
		out.setServiceDefinition(in.getServiceDefinition());
		out.setServiceGroup(in.getServiceGroup());

		return out;
	}

	protected static eu.arrowhead.common.model.ArrowheadService convertToDTO(
		database.model.ArrowheadService in) {

		eu.arrowhead.common.model.ArrowheadService out = new eu.arrowhead.common.model.ArrowheadService();

		out.setInterfaces(in.getInterfaces());
		out.setServiceDefinition(in.getServiceDefinition());
		out.setServiceGroup(in.getServiceGroup());

		return out;
	}

	protected static List<ArrowheadService> convertToDTO_ArrowheadServices(
		List<database.model.ArrowheadService> in) {

		if (in == null) {
			return null;
		}
		List<ArrowheadService> out = new ArrayList<>();
		for (database.model.ArrowheadService system : in) {
			out.add(convertToDTO(system));
		}

		return out;
	}

}
