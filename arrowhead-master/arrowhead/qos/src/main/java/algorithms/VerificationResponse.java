/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

/**
 *
 * @author Paulo
 */
public class VerificationResponse {

	private Boolean isPossible;
	private String reason;

	public VerificationResponse() {
	}

	public VerificationResponse(Boolean isPossible, String reason) {
		this.isPossible = isPossible;
		this.reason = reason;
	}

	public Boolean getIsPossible() {
		return isPossible;
	}

	public void setIsPossible(Boolean isPossible) {
		this.isPossible = isPossible;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
